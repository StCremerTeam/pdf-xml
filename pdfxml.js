'use strict'
const express = require('express'),
    http = require('http'),
    routes=require('./routes'),
    path = require('path'),
    app = express(),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    pug=require('pug')

app.disable('x-powered-by');

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.use(require('./routes/index'))

const server = http.createServer(app).listen(8124, function() {

    console.log('server running at port 8124');
});
