const PDFDocument = require('pdfkit'),
    fs = require('fs'),
    path = require('path'),
    header = [{
        text: 'Директору Г.К. "Лад"',
        style: {
            align: 'right',
            width: 450
        }
    }, {
        text: 'строка заголовка 2',
        style: {
            align: 'right',
            width: 450
        }
    }, {
        text:'строка заголовка 3',
        style: {
            align: 'right',
            width: 450
        }
    }],
    Form1 = [{
        text: 'Обращение',
        style: {
            align: 'center',
            width: 450,
            indent: 4000
        }
    }, {
        text: 'Задача организации, в особенности же начало повседневной работы по формированию позиции требуют определения и уточнения модели развития. Товарищи! реализация намеченных плановых заданий влечет за собой процесс внедрения и модернизации существенных финансовых и административных условий. Не следует, однако забывать, что дальнейшее развитие различных форм деятельности позволяет выполнять важные задания по разработке направлений прогрессивного развития. Таким образом новая модель организационной деятельности влечет за собой процесс внедрения и модернизации модели развития. С другой стороны начало повседневной работы по формированию позиции требуют определения и уточнения соответствующий условий активизации. Повседневная практика показывает, что укрепление и развитие структуры требуют от нас анализа дальнейших направлений развития.',

        style: {
            align: 'justify',
            width: 450
        }
    }],
    Form2 = [{
        text: 'директору г.к.Successgroup',
    }],
    Form3 = [{

    }]

let layout




module.exports.file = (req, res) => {
    console.log(req.body, '<-req.body')

    let doc = new PDFDocument,
        writeStream = fs.createWriteStream(path.join(__dirname + '/../public/docs/output.pdf'))
    doc.pipe(writeStream)
    doc.font('/usr/share/fonts/open-sans/OpenSans-Regular.ttf');
    doc.fontSize(10)
    switch (req.body.layout) {
        case ('Form1'):
            layout = header.concat(Form1)
            break
        case ('Form2'):
            layout = header.concat(Form2)
            break
        case ('Form3'):
            layout = header.concat(Form3)
            break
        default:
            res.status(403).res.send('bad request')
    }


    layout.map(function(field) {
        // doc.text(field.text)

        doc.text(field.text, (field.style) ? field.style : null)
    })

    doc.text(new Date().toDateString(), { width: 450, align: 'right' })
    doc.text(req.body.data.name + req.body.data.surname, { width: 450, align: 'left' })
        // doc.text(req.body.data.surname, { width: 450, align: 'right' })
    doc.end()
    writeStream.on('finish', function() {
        res.set({
            'Content-Type': 'application/pdf'
        })
        res.sendFile(path.join(__dirname + '/../public/docs/output.pdf'))
    });


    console.log(doc.end)
}
