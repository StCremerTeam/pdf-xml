'use strict'
const router = require('express').Router(),
    pdfGen = require('../models/pdfGenerate'),
    xmlGen = require('../models/xmlGenerate')

router.get('/',(req,res)=>{
	res.render('index')
})
router.post('/getpdfdoc', pdfGen.file)
// router.post('/getxmldoc', xmlGen.file)

module.exports = router
