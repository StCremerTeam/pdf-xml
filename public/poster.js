'use strict'
var sendReq = function(ev) {
    var formName = ev.target.form,
        sendData = {
            format: formName.format.value,
            layout: formName.layout.value,
            data: {
                name: formName.name.value,
                surname: formName.surname.value
            }
        },
        direction
    if (sendData.format === 'XML') { direction = '/getxmldoc' } else if (sendData.format === 'PDF') { direction = '/getpdfdoc' }
    if (direction) {
        var xhr = new XMLHttpRequest()
        xhr.open('POST', direction)
        xhr.responseType = 'arraybuffer'
            // xhr.onreadystatechange = function() {console.log(xhr.response)}
        xhr.setRequestHeader('content-type', 'application/json')
        xhr.send(JSON.stringify(sendData))
    }
    console.log(direction)
    console.log(sendData)
}

document.getElementById('GetDoc').addEventListener('click', sendReq)
